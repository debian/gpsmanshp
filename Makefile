#   Makefile for  gpsmanshp.c (Tcl 8.4 except 8.4.4)
#
#    A layer for writing GPS data to Shapefile format files
#    using shapelib from Tcl, as a Tcl package
#
#    This program was developed for use with
#    gpsman --- GPS Manager: a manager for GPS receiver data
#
#    Copyright (c) 2003-2013 Miguel Filgueiras (migfilg@t-online.de)
#
#     This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program.
#

VERSION = 1.2.3

TCLVERSION = 8.4

INSTALLDIR = /usr/lib/tcl$(TCLVERSION)

CFLAGS = -Wall -fPIC -c -I/usr/include/tcl$(TCLVERSION)
LINKOPT = -lshp -ltcl$(TCLVERSION)

gpsmanshp.so: gpsmanshp.o
	$(CC) -shared -o gpsmanshp.so $(LINKOPT) gpsmanshp.o

gpsmanshp.o: gpsmanshp.c
	$(CC) $(CFLAGS) gpsmanshp.c

pkgIndex.tcl: gpsmanshp.so
	echo "pkg_mkIndex -lazy -verbose . gpsmanshp.so" | tclsh$(TCLVERSION)
	chmod 644 gpsmanshp.so pkgIndex.tcl

install: pkgIndex.tcl
	-mkdir $(INSTALLDIR)
	cp gpsmanshp.so pkgIndex.tcl $(INSTALLDIR)

clean:
	rm -f gpsmanshp.o gpsmanshp.so pkgIndex.tcl G*.aux G*.log

www : GPSManSHP.tex
	hyperlatex GPSManSHP.tex
	chmod 644 gpsmanshp-www/GPSManSHP*.html
#	scp -p gpsmanshp-www/GPSManSHP*.html gpsmanshp@ssh:public_html

distrib: GPSManSHP.tex gpsmanshp.c gpstr2shp.c Makefile
	-mkdir -m 755 gpsmanshp_$(VERSION)
	rm -fr gpsmanshp_$(VERSION)/*
	cp gpsmanshp.c gpstr2shp.c Makefile Makefile*[0-9] gpsmanshp_$(VERSION)
	chmod 644 gpsmanshp_$(VERSION)/*
	mkdir -m 755 gpsmanshp_$(VERSION)/doc
	pdflatex GPSManSHP.tex
	pdflatex GPSManSHP.tex
	cp GPSManSHP.pdf gpsmanshp_$(VERSION)/doc
#	hyperlatex GPSManSHP.tex
#	cp gpsmanshp-www/GPSManSHP*.html gpsmanshp-www/*.gif gpsmanshp-www/GPL.txt gpsmanshp_$(VERSION)/doc
	chmod 644 gpsmanshp_$(VERSION)/doc/*
	tar czvf gpsmanshp_$(VERSION).tgz gpsmanshp_$(VERSION)
#	zip -r gpsmanshp_$(VERSION).zip gpsmanshp_$(VERSION)
#	chmod 644 gpsmanshp_$(VERSION).tgz gpsmanshp_$(VERSION).zip
	chmod 644 gpsmanshp_$(VERSION).tgz
#	scp -p gpsmanshp-www/GPSManSHP*.html gpsmanshp_$(VERSION).tgz gpsmanshp_$(VERSION).zip gpsmanshp@ssh:public_html
	sha1sum gpsmanshp_$(VERSION).tgz > distrib/gpsmanshp_$(VERSION).tgz.sha1sum
	mv gpsmanshp_$(VERSION).tgz distrib

